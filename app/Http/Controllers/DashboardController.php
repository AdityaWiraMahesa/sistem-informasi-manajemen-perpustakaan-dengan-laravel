<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class DashboardController extends Controller
{
    public function dashboard(){
        $mahasiswa = DB::table('mahasiswa')->get();
        $buku = DB::table('buku')->get();
        $transaksi = DB::table('transaksi')->get();
        $tot_buku = DB::table('buku')->count();
		return view('dashboard',[
            'mahasiswa' => $mahasiswa,
            'buku' => $buku,
            'transaksi' => $transaksi,
            'stok_buku' => $tot_buku,
            "title" => "Dashboard"
        ]);
    }
}
