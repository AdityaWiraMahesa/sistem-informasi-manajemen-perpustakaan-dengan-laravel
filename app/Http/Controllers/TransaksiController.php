<?php

namespace App\Http\Controllers;

use DateTime;
use DateTimeZone;
use Illuminate\Support\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class TransaksiController extends Controller
{
    public function transaksi(){
        $transaksi = DB::table('transaksi')
        ->join('mahasiswa', 'transaksi.id_mahasiswa', '=', 'mahasiswa.id')
        ->join('buku', 'transaksi.id_buku', '=', 'buku.id')
        ->select('transaksi.*', 'mahasiswa.nama', 'buku.judul_buku', 'isbn', 'stok_buku', 'transaksi.id')
        ->orderBy('transaksi.id')
        ->get();
		return view('transaksi.transaksi',[
            'transaksi' => $transaksi,
            "title" => "Data transaksi"
        ]);
    }

    public function tambah(){
        $buku = DB::table('buku')->get();
        $mahasiswa = DB::table('mahasiswa')->get();
		return view('transaksi.tambah', compact('buku', 'mahasiswa'),[
            "title" => "Tambah Data Transaksi"
        ]);
    }
    
    public function add(Request $request)
	{
        // $tanggal_pinjam = new DateTime(now('Asia/Jakarta'));
        $tgl_pinjam = Carbon::now('Asia/Jakarta');
		DB::table('transaksi')->insert([
			'id_mahasiswa' => $request->mahasiswa,
			'id_buku' => $request->buku,
            'tanggal_pinjam' => $tgl_pinjam->toDateTimeString()
		]);
        $jml_buku = DB::table('buku')->where('id', $request->buku)->select('stok_buku')->get();
        $total = $jml_buku[0]->stok_buku - 1;
            // dd($total);
        DB::table('buku')->where('id', $request['buku'])->update(['stok_buku' => $total]);
		return redirect('/transaksi');
	}

    public function edit($id)
	{
        $buku = DB::table('buku')->get();
        $mahasiswa = DB::table('mahasiswa')->get();
		$transaksi = DB::table('transaksi')->where('id',$id)->get();
		return view('transaksi.edit', compact('buku', 'mahasiswa'),[
            'transaksi' => $transaksi,
            "title" => "Edit Data Transaksi"
        ]);
	}
	public function update(Request $request)
	{   
		DB::table('transaksi')->where('id',$request->id)->update([
			'id_mahasiswa' => $request->mahasiswa,
			'id_buku' => $request->buku,
		]);
		return redirect('/transaksi');
	}

	public function hapus($id)
	{
		DB::table('transaksi')->where('id',$id)->delete();
		return redirect('/transaksi');
	}

	public function update_status(Request $request)
    {   
        // $tanggal_kembali = new DateTime(now('Asia/Jakarta'));
        $tgl_kembali = Carbon::now('Asia/Jakarta');
        // $tgl_pinjam = DB::table('transaksi')->where('id',$request->id)->select('tanggal_pinjam')->get();
        // $date1 = $tgl_pinjam;
        // $date2 = $tgl_kembali;
        // $difference = $date1->diff($date2);
        // $tanggal_kembali = new DateTime($tgl_kembali);
        // $tanggal_pinjam = new DateTime($tgl_pinjam);
        // $interval = $tanggal_pinjam->diff($tanggal_kembali);
        // s
        // $data = DB::table('transaksi')->where('id', '=', $request->id)->select(DB::raw('datediff(tanggal_kembali,tanggal_pinjam) as total'))->get();
        // $lama = $tanggal_pinjam->date_diff($tanggal_kembali);
        // $hari = $lama->d;
        // $biaya_harian = DB::table('transaksi')->where('transaksi.id',$request->id)
        // ->join('buku', 'transaksi.id_buku', '=', 'buku.id')                
        // ->select('biaya_sewa_harian')
        // ->get();
        // $total = $biaya_harian * $difference;
        DB::table('transaksi')->where('id', $request->id)->update([
            'tanggal_kembali' => $tgl_kembali,
            // 'total_biaya' => $biaya_harian,
            'status_pinjam' => 'dikembalikan'
        ]);
        // Total Buku
        $jmlh = DB::table('transaksi')
            ->where([['transaksi.id', '=', $request->id]])
            ->join('buku', 'transaksi.id_buku', '=', 'buku.id')
            ->select('buku.*', 'stok_buku')->first();
        $total = $jmlh->stok_buku + 1;
        DB::table('buku')->where("id", $jmlh->id)->update(['stok_buku' => $total]);
        return redirect('/transaksi');
    }
}