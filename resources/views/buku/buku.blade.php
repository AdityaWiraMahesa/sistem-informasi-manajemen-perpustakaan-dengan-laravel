@extends('layouts.main')

@section('konten')
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0">{{ $title }}</h1>
            </div>
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <div class="card-title"><a href="/btambah" class="btn btn-sm btn-primary float-left"><i
                                    class="fas fa-plus"></i>
                                Tambah Data Buku</a></div>
                        <div class="card-tools">
                            <div class="input-group input-group-sm" style="width: 150px;">
                                <input type="text" name="table_search" class="form-control float-right"
                                    placeholder="Search">
                                <div class="input-group-append">
                                    <button type="submit" class="btn btn-default">
                                        <i class="fas fa-search"></i>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body table-responsive p-0" style="height: 300px;">
                        <table class="table table-head-fixed text-nowrap">
                            <thead>
                                <tr class="odd">
                                    <th>No</th>
                                    <th>Judul Buku</th>
                                    <th>Pengarang</th>
                                    <th>Penerbit</th>
                                    <th>Tahun Terbit</th>
                                    <th>Tebal</th>
                                    <th>ISBN</th>
                                    <th>Stok Buku</th>
                                    <th>Biaya Sewa Harian</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($buku as $bk)
                                <tr>
                                    {{-- <td>{{ $mhs->id }}</td> --}}
                                    <td>{{ $loop->iteration }}</td>
                                    <td>{{ $bk->judul_buku }}</td>
                                    <td>{{ $bk->pengarang }}</td>
                                    <td>{{ $bk->penerbit }}</td>
                                    <td>{{ $bk->tahun_terbit }}</td>
                                    <td>{{ $bk->tebal }}</td>
                                    <td>{{ $bk->isbn }}</td>
                                    <td>{{ $bk->stok_buku }}</td>
                                    <td>Rp. {{ $bk->biaya_sewa_harian }}</td>
                                    <td>
                                        <a href="/bedit/{{ $bk->id }}" class="fas fa-edit"></a>
                                        <a href="/bhapus/{{ $bk->id }}" class="fas fa-trash-alt" style="color:red"></a>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
            </div>
        </div>
    </div>
</section>

@endsection