@extends('layouts.main')
@section('konten')
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0">{{ $title }}</h1>
            </div>
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card card-light">
                    <!-- form start -->
                    @foreach ($buku as $bk)
                    <form action="/bupdate" method="post">
                        {{ csrf_field() }}
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-5">
                                    <div class="form-group">
                                        <label for="">Judul Buku</label>
                                        <input type="text" class="form-control" name="judul"
                                        value="{{ $bk->judul_buku }}">
                                    </div>
                                    <div class="form-group">
                                        <label for="">Pengarang</label>
                                        <input type="text" class="form-control" name="pengarang"
                                        value="{{ $bk->pengarang }}">
                                    </div>
                                    <div class="form-group">
                                        <label for="">Penerbit</label>
                                        <input type="text" class="form-control" name="penerbit"
                                        value="{{ $bk->penerbit }}">
                                    </div>
                                    <div class="form-group">
                                        <label for="">Tahun Terbit</label>
                                        <input type="number" class="form-control" name="tahun"
                                        value="{{ $bk->tahun_terbit }}">
                                    </div>
                                </div>
                                <div class="col-md-5 ml-4">
                                    <div class="form-group">
                                        <label for="">Tebal Buku</label>
                                        <input type="number" class="form-control" name="tebal"
                                        value="{{ $bk->tebal }}">
                                    </div>
                                    <div class="form-group">
                                        <label for="">ISBN</label>
                                        <input type="text" class="form-control" name="isbn"
                                        value="{{ $bk->isbn }}">
                                    </div>
                                    <div class="form-group">
                                        <label for="">Stok Buku</label>
                                        <input type="text" class="form-control" name="stok"
                                        value="{{ $bk->stok_buku }}">
                                    </div>
                                    <div class="form-group">
                                        <label for="">Biaya Sewa Harian</label>
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text">Rp.</span>
                                            </div>
                                            <input type="text" class="form-control" name="biaya"
                                            value="{{ $bk->biaya_sewa_harian }}">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <input type="hidden" class="form-control" name="id" value="{{ $bk->id }}">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /.card-body -->

                        <div class="card-footer">
                            <div class="row">
                                <div class="form-group pl-3">
                                    <a href="/buku" class="btn btn-danger">Batal</a>
                                    <button type="submit" class="btn btn-primary">Update</button>
                                </div>
                            </div>
                        </div>
                    </form>
                    @endforeach
                </div>
            </div>
        </div>
    </div>

</section>

@endsection

