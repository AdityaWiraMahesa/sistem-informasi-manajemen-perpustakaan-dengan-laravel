@extends('layouts.main')
@section('konten')
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0">{{ $title }}</h1>
            </div>
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card card-light">
                    <!-- form start -->
                    <form action="/madd" method="post">
                        {{ csrf_field() }}
                        <div class="card-body">
                            <div class="row">
                                <div class="form-group col-md-5">
                                    <div class="form-group">
                                        <label for="">Nama</label>
                                        <input type="text" class="form-control" name="nama" autocomplete="off"
                                            placeholder="Masukan Nama">
                                    </div>
                                    <div class="form-group">
                                        <label for="">NIM</label>
                                        <input type="number" class="form-control" name="nim"
                                            placeholder="Masukan NIM">
                                    </div>
                                    <div class="form-group">
                                        <label for="">Email</label>
                                        <input type="email" class="form-control" name="email"
                                            placeholder="Masukan Email">
                                    </div>
                                    <div class="form-group">
                                        <label for="">No Telp</label>
                                        <input type="text" class="form-control" name="no"
                                            placeholder="Masukan No Telp">
                                    </div>
                                </div>
                                <div class="form-group col-md-5 ml-4">
                                    <div class="form-group">
                                        <label for="">Prodi</label>
                                        <input type="text" class="form-control" name="prodi"
                                            placeholder="Masukan Prodi">
                                    </div>
                                    <div class="form-group">
                                        <label for="">Jurusan</label>
                                        <input type="text" class="form-control" name="jurusan"
                                            placeholder="Masukan Jurusan">
                                    </div>
                                    <div class="form-group">
                                        <label for="">Fakultas</label>
                                        <input type="text" class="form-control" name="fakultas"
                                            placeholder="Masukan Fakultass">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /.card-body -->

                        <div class="card-footer">
                            <div class="row">
                                <div class="form-group pl-3">
                                    <a href="/mahasiswa" class="btn btn-danger">Batal</a>
                                    <button type="submit" class="btn btn-primary">Save</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection