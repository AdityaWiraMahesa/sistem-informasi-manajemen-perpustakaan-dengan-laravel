@extends('layouts.main')
@section('konten')
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0">{{ $title }}</h1>
            </div>
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card card-light">
                    <!-- form start -->
                    <form action="/tupdate" method="post">
                        {{ csrf_field() }}
                        <div class="card-body">
                            <div class="row">
                                <div class="form-group col-md-5">
                                    <div class="form-group">
                                        <label> Judul Buku</label>
                                        <select class="form-control" id="buku" type="text" name="buku" required>
                                            <option>- Pilih -</option>
                                            @foreach ($buku as $buku)
                                            @if ($buku->stok_buku <= 0)
                                            <option disabled>{{$buku->id}} - {{$buku->judul_buku}}</option>
                                            @else
                                            @foreach ($transaksi as $trs)
                                            <option value="{{$buku->id}}" {{ $buku->id == $trs->id_buku ? 'selected' : '' }}>{{$buku->id}} - {{$buku->judul_buku}}
                                            </option>
                                            @endforeach
                                            @endif
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>Nama Mahasiswa</label>
                                        <select class="form-control" id="mahasiswa" type="text" name="mahasiswa" required>
                                            <option>- Pilih -</option>
                                            @foreach ($mahasiswa as $mhs)
                                                @foreach ($transaksi as $trs)
                                                <option value="{{$mhs->id}}" {{ $mhs->id == $trs->id_mahasiswa ? 'selected' : '' }}>{{$mhs->nama}}</option>
                                                @endforeach
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <input type="hidden" class="form-control" name="id" value="{{ $trs->id }}">
                                    </div>
                                    {{-- <div class="form-group">
                                        <label for="">Id Mahasiswa</label>
                                        <input type="text" class="form-control" name="id_mhs"
                                            placeholder="Masukan ID Mahasiswa">
                                    </div>
                                    {{-- <div class="form-group">
                                        <label for="">Nama Mahasiswa</label>
                                        <input type="text" class="form-control" name="nama_mhs"
                                            placeholder="Masukan Nama Mahasiswa">
                                    </div> --}}
                                    {{-- <div class="form-group">
                                        <label for="">Id Buku</label>
                                        <input type="number" class="form-control" name="id_buku"
                                            placeholder="Masukan ID Buku">
                                    </div> --}}
                                    {{-- <div class="form-group">
                                        <label for="">Judul Buku</label>
                                        <input type="text" class="form-control" name="judul_buku"
                                            placeholder="Masukan Judul Buku">
                                    </div> --}}
                                    {{-- <div class="form-group">
                                        <label for="">ISBN</label>
                                        <input type="text" class="form-control" name="isbn"
                                            placeholder="Masukan NIM">
                                    </div> --}}
                                </div>
                            </div>
                        </div>
                        <!-- /.card-body -->

                        <div class="card-footer">
                            <div class="row">
                                <div class="form-group pl-3">
                                    <a href="/transaksi" class="btn btn-danger">Batal</a>
                                    <button type="submit" class="btn btn-primary">Save</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    
</section>
@endsection