@extends('layouts.main')

@section('konten')
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0">{{ $title }}</h1>
            </div>
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <div class="card-title"><a href="/ttambah" class="btn btn-sm btn-primary float-left"><i
                                    class="fas fa-plus"></i>
                                Tambah Data Transaksi</a></div>
                        <div class="card-tools">
                            <div class="input-group input-group-sm" style="width: 150px;">
                                <input type="text" name="table_search" class="form-control float-right"
                                    placeholder="Search">
                                <div class="input-group-append">
                                    <button type="submit" class="btn btn-default">
                                        <i class="fas fa-search"></i>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <!-- /.card-header -->
                    <div class="card-body table-responsive p-0" style="height: 300px;">
                        <table class="table table-head-fixed text-nowrap">
                            <thead>
                                <tr class="odd">
                                    <th>No</th>
                                    <th>Nama</th>
                                    <th>Judul</th>
                                    <th>ISBN</th>
                                    <th>Tgl Pinjam</th>
                                    <th>Tgl Kembali</th>
                                    <th>Status</th>
                                    <th>Biaya</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($transaksi as $trs)
                                <tr>
                                    {{-- <td>{{ $mhs->id }}</td> --}}
                                    <td>{{ $loop->iteration }}</td>
                                    <td>{{ $trs->nama }}</td>
                                    <td>{{ $trs->judul_buku }}</td>
                                    <td>{{ $trs->isbn }}</td>
                                    <td>{{ $trs->tanggal_pinjam }}</td>
                                    @if($trs->tanggal_kembali == NULL)
                                        <td class="text-center">-</td>
                                    @else
                                        <td>{{$trs->tanggal_kembali}} </td>
                                    @endif
                                    <td>
                                        @if ($trs ->status_pinjam == 'dipinjam')
                                        <div class="btn-group">
                                            <button type="button" class="btn btn-info btn-sm">{{ $trs->status_pinjam }}</button>
                                            <button type="button" class="btn btn-info dropdown-toggle dropdown-toggle-split btn-sm" data-bs-toggle="dropdown" aria-expanded="false">
                                              <span class="visually-hidden"></span>
                                            </button>
                                            <ul class="dropdown-menu">
                                                <li><a class="dropdown-item" id="dikembalikan" href="/update_status/{{ $trs->id }}"
                                                    >Dikembalikan</a></li>
                                            </ul>
                                        </div>
                                        @else 
                                        <div class="btn-group">
                                            <button type="button" class="btn btn-danger btn-sm">{{ $trs->status_pinjam }}</button>
                                        </div>
                                        @endif
                                    </td>
                                    <td>Rp. {{ $trs->total_biaya }}</td>
                                    <td>  
                                        @if ($trs ->status_pinjam == 'dipinjam')
                                        <a href="/tedit/{{ $trs->id }}" class="fas fa-edit"></a>
                                        @else
                                        <a href="" class="fas fa-edit" data-bs-toggle="modal" data-bs-target="#Modal_Edit"></a>
                                        @endif
                                        @if ($trs ->status_pinjam == 'dipinjam')
                                        <a href="" class="fas fa-trash-alt" style="color:red" data-bs-toggle="modal" data-bs-target="#Modal_Delete"></a>
                                        @else
                                        <a href="/thapus/{{ $trs->id }}" class="fas fa-trash-alt" style="color:red"></a>
                                        @endif
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
            </div>
        </div>
    </div>

    
    <!-- Modal Ubah Status -->
    {{-- <div class="modal fade" id="staticBackdrop" data-bs-backdrop="/update_status" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
        <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
            <h5 class="modal-title" id="staticBackdropLabel"><b>Informasi Transaksi</b></h5>
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <table>
                    <tbody>
                        <tr>
                            <td>Nama</td><td>:</td><td><span id="nama"></span></td>
                        </tr>
                        <tr>
                            <td>Judul Buku</td><td>:</td><td>{{ $trs->judul_buku }}</td>
                        </tr>
                        <tr>
                            <td>ISBN</td><td>:</td><td>{{ $trs->isbn }}</td>
                        </tr>
                        <tr>
                            <td>Tanggal Pinjam</td><td>:</td><td>{{ $trs->tanggal_pinjam }}</td>
                        </tr>
                        <tr>
                            <td>Total Biaya</td><td>:</td><td>Rp.</td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
            <button type="button" class="btn btn-primary" >Save</button>
            </div>
        </div>
        </div>
    </div> --}}
  
    <!-- Modal Delete-->
    <div class="modal fade" id="Modal_Delete" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
            <h5 class="modal-title" id="staticBackdropLabel"><b>Informasi</b></h5>
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                Maaf Transaksi tidak dapat di Hapus
            </div>
            <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
            </div>
        </div>
        </div>
    </div>
    {{-- Modal Edit --}}
    <div class="modal fade" id="Modal_Edit" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
            <h5 class="modal-title" id="staticBackdropLabel"><b>Informasi</b></h5>
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                Maaf Transaksi tidak dapat di Update
            </div>
            <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
            </div>
        </div>
        </div>
    </div>
    {{-- <script>
        $(document).ready(function(){
            $(document).on('click', '#dikembalikan', function(){
                var nama = $(this).data('nama');
                var judul = $(this).data('judul');
                var isbn = $(this).data('isbn')
                var tglpinjam = $(this).data('tglpinjam')
                var tglkembali = $(this).data('tglkembali')
                $('#nama').text(nama);
                $('#judul').text(judul);
                $('#isbn').text(isbn);
                $('#tglpinjam').text(tglpinjam);
                $('#tglkembali').text(tglkembali);
            })
        })
    </script> --}}
</section>

@endsection

